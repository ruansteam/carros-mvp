package br.com.mvp.carros.carrosmvp.features.carros.listar.adapter

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import br.com.mvp.carros.carrosmvp.R
import br.com.mvp.carros.carrosmvp.features.carros.listar.fragment.CarrosFragment
import br.com.mvp.carros.carrosmvp.features.carros.listar.fragment.FavoritosFragment

/**
 * Created by Ruan Correa on 05/01/18.
 */
class TabsAdapter(private val context: Context, fm: FragmentManager) : FragmentPagerAdapter(fm) {
    // Qtde de Tabs
    override fun getCount() = 4

    // Retorna o tipo pela posição
    fun getTipoCarro(position: Int) = when (position) {
        0 -> TipoCarro.CLASSICOS
        1 -> TipoCarro.ESPORTIVOS
        2 -> TipoCarro.LUXO
        else -> TipoCarro.FAVORITOS
    }

    // Título da Tab
    override fun getPageTitle(position: Int): CharSequence {
        val tipo = getTipoCarro(position)
        return context.getString(tipo.string)
    }

    // Fragment com a lista de carros
    override fun getItem(position: Int): Fragment {
        if (position == 3) {
            // Favoritos
            return FavoritosFragment()
        }
        // Clássicos, Esportivos e Luxo
        val tipo = getTipoCarro(position)
        val f: Fragment = CarrosFragment()
        f.arguments = Bundle()
        f.arguments?.putSerializable("tipo", tipo)
        return f
    }
}

enum class TipoCarro(val string: Int) {
    CLASSICOS(R.string.classicos),
    ESPORTIVOS(R.string.esportivos),
    LUXO(R.string.luxo),
    FAVORITOS(R.string.favoritos)
}
