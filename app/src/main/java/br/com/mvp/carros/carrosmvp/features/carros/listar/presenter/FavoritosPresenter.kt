package br.com.mvp.carros.carrosmvp.features.carros.listar.presenter

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import br.com.mvp.carros.carrosmvp.R
import br.com.mvp.carros.carrosmvp.common.util.getDB
import br.com.mvp.carros.carrosmvp.database.entitiy.Carro
import br.com.mvp.carros.carrosmvp.features.carros.listar.adapter.TipoCarro
import br.com.mvp.carros.carrosmvp.features.carros.listar.domain.vo.CarroVO
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlin.properties.Delegates

/**
 * Created by Ruan Correa on 05/01/18.
 */
class FavoritosPresenter(val viewCallback: FavoritosPresenter.ViewCallback) {

    private lateinit var tipo: TipoCarro

    private var list: MutableList<Carro> by Delegates.observable(mutableListOf()) { prop, old, new ->
        viewCallback.updateAdapter(CarroVO.fromDb(list), action = { carro, imageView -> viewCallback.showActivityDetalhesCarro(carro, imageView) })
    }

    fun onViewCreated(view: android.view.View?, savedInstanceState: Bundle?) {
        this.viewCallback.setupViews()

        updateList()
    }

    private fun updateList() {
        viewCallback.showOrHideSemRegistros(View.GONE)
        getDB()?.carroDAO()?.findAll()
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeBy(onNext = { list ->
                    this.list = list.toMutableList()
                    viewCallback.closeSwipeRefresh()
                    if (list.isEmpty()) {
                        viewCallback.showOrHideSemRegistros(View.VISIBLE)
                        return@subscribeBy
                    }
                    viewCallback.showOrHideSemRegistros(View.GONE)
                }, onError = { error ->
                    viewCallback.showToastError(R.string.erro)
                    viewCallback.closeSwipeRefresh()
                    viewCallback.showOrHideSemRegistros(View.VISIBLE)
                })
    }

    fun swipeToRefreshListener() {
        updateList()
    }

    interface ViewCallback {
        fun setupViews()
        fun updateAdapter(list: MutableList<CarroVO>, action: (CarroVO, ImageView) -> Unit)
        fun closeSwipeRefresh()
        fun showOrHideSemRegistros(visible: Int)
        fun showToastError(erro: Int)
        fun showActivityDetalhesCarro(carro: CarroVO, imageView: ImageView)
    }
}

