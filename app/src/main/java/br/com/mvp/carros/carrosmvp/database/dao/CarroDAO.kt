package br.com.mvp.carros.carrosmvp.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import br.com.mvp.carros.carrosmvp.database.entitiy.Carro
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single

/**
 * Created by Ruan Correa on 05/01/18.
 */
@Dao
interface CarroDAO {
    @Query("SELECT * FROM carro where id = :id")
    fun callGetById(id: Long): Flowable<Carro>

    @Query("SELECT * FROM carro where id = :id")
    fun getById(id: Long): Carro?

    @Query("SELECT * FROM carro")
    fun findAll(): Flowable<List<Carro>>

    @Query("SELECT count(id) FROM carro LIMIT 1")
    fun findAllSingle(): Single<Boolean>

    @Insert
    fun insert(carro: Carro)

    @Delete
    fun delete(carro: Carro)

    @Query("SELECT count(id) FROM carro WHERE id = :id")
    fun isFavorito(id: Long): Boolean
}
