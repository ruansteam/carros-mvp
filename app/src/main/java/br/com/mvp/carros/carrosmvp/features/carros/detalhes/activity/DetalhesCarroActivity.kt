package br.com.mvp.carros.carrosmvp.features.carros.detalhes.activity

import android.content.Intent
import android.content.res.ColorStateList
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.MenuItem
import br.com.mvp.carros.carrosmvp.R
import br.com.mvp.carros.carrosmvp.features.carros.detalhes.presenter.DetalhesCarroPresenter
import br.com.mvp.carros.carrosmvp.features.carros.listar.domain.vo.CarroVO
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import kotlinx.android.synthetic.main.activity_detalhes_carro.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.toast

class DetalhesCarroActivity : AppCompatActivity(), DetalhesCarroPresenter.ViewCallback {

    val presenter: DetalhesCarroPresenter by lazy { DetalhesCarroPresenter(this)}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalhes_carro)
        presenter.onCreate(savedInstanceState = savedInstanceState, extra = intent.extras)
        fabFavoritar.onClick { presenter.onClickFavoritar() }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                supportFinishAfterTransition()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
    }

    override fun setupViews(carro: CarroVO?) {
        tDesc.text = carro?.desc
        Glide.with(this).load(carro?.urlFoto).transition(withCrossFade()).into(img)
        Glide.with(this).load(carro?.urlFoto).transition(withCrossFade()).into(imgVideo)

        // Toca o Vídeo
        imgPlayVideo.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.setDataAndType(Uri.parse(carro?.urlVideo), "video/*")
            startActivity(intent)
        }

        // Adiciona o fragment do Mapa
//        val mapaFragment = MapaFragment()
//        mapaFragment.arguments = intent.extras
//        supportFragmentManager
//                .beginTransaction()
//                .replace(R.id.mapaFragment, mapaFragment)
//                .commit()
    }

    override fun setFavoritarCor(favoritado: Boolean) {
        val fundo = ContextCompat.getColor(this, if (favoritado) R.color.favorito_on else R.color.favorito_off)
        val cor = ContextCompat.getColor(this, if (favoritado) R.color.yellow else R.color.favorito_on)
        fabFavoritar.backgroundTintList = ColorStateList(arrayOf(intArrayOf(0)), intArrayOf(fundo))
        fabFavoritar.setColorFilter(cor)
    }

    override fun showToast(message: Int) {
        toast(message)
    }
}
