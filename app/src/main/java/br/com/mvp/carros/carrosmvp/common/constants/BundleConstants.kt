package br.com.mvp.carros.carrosmvp.common.constants

/**
 * Created by Ruan Correa on 08/01/18.
 */
class BundleConstants {
    companion object {
        const val BUNDLE_CARRO = "bundle_carro"
        const val TIPO: String = "tipo"
    }
}