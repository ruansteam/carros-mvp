package br.com.mvp.carros.carrosmvp.database.entitiy

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import br.com.mvp.carros.carrosmvp.features.carros.listar.domain.vo.CarroVO

/**
 * Created by Ruan Correa on 05/01/18.
 */
@Entity
class Carro {
    @PrimaryKey
    var id: Long = 0
    var tipo: String? = null
    var nome: String? = null
    var desc: String? = null

    @ColumnInfo(name = "url_foto")
    var urlFoto: String? = null
    @ColumnInfo(name = "url_info")
    var urlInfo: String? = null
    @ColumnInfo(name = "url_video")
    var urlVideo: String? = null

    companion object {
        fun from(carroVO: CarroVO): Carro {
            var carro = Carro()
            carro.id = carroVO.id
            carro.tipo = carroVO.tipo
            carro.nome = carroVO.nome
            carro.desc = carroVO.desc
            carro.urlFoto = carroVO.urlFoto
            carro.urlInfo = carroVO.urlInfo
            carro.urlVideo = carro.urlVideo
            return carro
        }
    }
}