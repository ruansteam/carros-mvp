package br.com.mvp.carros.carrosmvp.features.carros.listar.domain.vo

import android.os.Parcelable
import br.com.mvp.carros.carrosmvp.database.entitiy.Carro
import kotlinx.android.parcel.Parcelize

/**
 * Created by Ruan Correa on 05/01/18.
 */
@Parcelize
data class CarroVO(var id: Long = 0,
                   var tipo: String? = null,
                   var nome: String? = null,
                   var desc: String? = null,
                   var urlFoto: String? = null,
                   var urlInfo: String? = null,
                   var urlVideo: String? = null): Parcelable {

    companion object {
        fun fromDb(list: List<Carro>): MutableList<CarroVO> {
            var listVo = mutableListOf<CarroVO>()
            list.mapTo(listVo) { CarroVO.fromDb(item = it) }
            return listVo
        }

        fun fromDb(item: Carro): CarroVO {
            var carro = CarroVO()
            carro.id = item.id
            carro.tipo = item.tipo
            carro.nome = item.nome
            carro.desc = item.desc
            carro.urlFoto = item.urlFoto
            carro.urlInfo = item.urlInfo
            carro.urlVideo = carro.urlVideo
            return carro
        }
    }
}