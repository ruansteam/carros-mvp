package br.com.mvp.carros.carrosmvp.application

import android.app.Application
import android.content.Context
import android.support.multidex.MultiDex
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import br.com.mvp.carros.carrosmvp.database.CarrosDatabase


/**
 * Created by Ruan Correa on 08/01/18.
 */
class CarrosApplication: Application() {

    var db: CarrosDatabase? = null

    companion object {
        lateinit var instance: CarrosApplication
            private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        db = Room.databaseBuilder(this, CarrosDatabase::class.java, "carros_database").build()
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)

        MultiDex.install(this)
    }
}