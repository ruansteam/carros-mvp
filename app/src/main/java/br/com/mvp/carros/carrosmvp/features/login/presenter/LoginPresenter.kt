package br.com.mvp.carros.carrosmvp.features.login.presenter

import android.os.Bundle
import br.com.mvp.carros.carrosmvp.R
import br.com.mvp.carros.carrosmvp.common.constants.WebServicesConstants
import br.com.mvp.carros.carrosmvp.common.util.createRetrofitService
import br.com.mvp.carros.carrosmvp.features.login.service.http.LoginService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

/**
 * Created by Ruan Correa on 04/01/18.
 */
class LoginPresenter(private val viewCallback: LoginPresenter.ViewCallback) {

    private val service: LoginService by lazy {
        createRetrofitService(WebServicesConstants.URL_LOGIN).create(LoginService::class.java)
    }

    fun onCreate(savedInstanceState: Bundle? = null) {
        viewCallback.setTitle(R.string.login)
    }

    fun onClickLogin(email: String, senha: String) {
        if (email.isEmpty() || senha.isEmpty()) {
            viewCallback.showError(R.string.usuario_senha_incorreto)
            return
        }

        viewCallback.showProgress()
        viewCallback.disableFields()
        service.doPostLogin(email)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(onNext = { list ->
                    viewCallback.hideProgress()
                    if (list.count() == 1) {
                        viewCallback.enableFields()
                        viewCallback.showMain()
                        return@subscribeBy
                    }
                    viewCallback.showError(R.string.erro)
                }, onError = {
                    viewCallback.hideProgress()
                    viewCallback.showError(R.string.erro)
                    viewCallback.enableFields()
                })
    }

    interface ViewCallback {
        fun showProgress()
        fun hideProgress()
        fun disableFields()
        fun enableFields()
        fun showError(messageResId: Int)
        fun setTitle(titleResId: Int)
        fun showMain()
    }
}