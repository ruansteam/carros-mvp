package br.com.mvp.carros.carrosmvp.features.carros.listar.activity

import android.content.res.Configuration
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v7.app.ActionBarDrawerToggle
import android.view.MenuItem
import br.com.mvp.carros.carrosmvp.R
import br.com.mvp.carros.carrosmvp.features.carros.criar.activity.CriarCarroActivity
import br.com.mvp.carros.carrosmvp.features.carros.listar.adapter.TabsAdapter
import br.com.mvp.carros.carrosmvp.features.carros.listar.presenter.MainPresenter
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.startActivity

class MainActivity : AppCompatActivity(), MainPresenter.ViewCallback {

    val presenter: MainPresenter by lazy { MainPresenter(this) }
    private lateinit var drawerToogle: ActionBarDrawerToggle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter.onCreate(savedInstanceState)

        fabFavoritar.setOnClickListener {
            presenter.onClickFab()
        }
    }


    override fun setupMenuButton() {
        setSupportActionBar(toolbar)
        drawerToogle = object: ActionBarDrawerToggle(this, drawer_layout, R.string.drawer_open, R.string.drawer_close) {}
        drawer_layout.addDrawerListener(drawerToogle)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
    }

    override fun showCriarCarro() {
        startActivity<CriarCarroActivity>()
    }

    override fun setupTabBar() {
        viewPager.offscreenPageLimit = 3
        viewPager.adapter = TabsAdapter(this, supportFragmentManager)
        tabLayout.setupWithViewPager(viewPager)
        val cor = ContextCompat.getColor(this, R.color.white)
        tabLayout.setTabTextColors(cor, cor)

        viewPager.addOnPageChangeListener(object: ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) { }
            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) { }
            override fun onPageSelected(page: Int) {
            }
        })
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        drawerToogle.syncState()
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        drawerToogle.onConfigurationChanged(newConfig)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (drawerToogle.onOptionsItemSelected(item)) {
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
