package br.com.mvp.carros.carrosmvp.features.carros.detalhes.presenter

import android.os.Bundle
import br.com.mvp.carros.carrosmvp.R
import br.com.mvp.carros.carrosmvp.common.constants.BundleConstants
import br.com.mvp.carros.carrosmvp.features.carros.detalhes.services.db.DetalhesCarroDBService
import br.com.mvp.carros.carrosmvp.features.carros.listar.domain.vo.CarroVO
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

/**
 * Created by Ruan Correa on 08/01/18.
 */
class DetalhesCarroPresenter(val viewCallback: DetalhesCarroPresenter.ViewCallback) {

    private var carroSelecionado: CarroVO? = null

    fun onCreate(savedInstanceState: Bundle?, extra: Bundle?) {
        viewCallback.setupToolbar()
        if (extra != null) {
            carroSelecionado = extra.getParcelable(BundleConstants.BUNDLE_CARRO)
        }

        viewCallback.setupViews(carro = carroSelecionado)
        callIsFavorito()
    }

    private fun callIsFavorito() {
        carroSelecionado?.let { carro ->
            Observable.fromCallable { DetalhesCarroDBService.isFavorito(carro) }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeBy(onNext = {
                        viewCallback.setFavoritarCor(it)
                    })
        }
    }

    fun onClickFavoritar() {
        carroSelecionado?.let { carro ->

            Observable.fromCallable { DetalhesCarroDBService.favoritarOuDesfavoritar(carro) }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeBy(onNext = { it ->
                        viewCallback.showToast(if (it) R.string.msg_carro_favoritado else R.string.msg_carro_desfavoritado)
                        viewCallback.setFavoritarCor(it)
                    }, onError = {
                        viewCallback.showToast(R.string.erro)
                    })
        }
    }

    interface ViewCallback {
        fun setupToolbar()
        fun setupViews(carro: CarroVO?)
        fun setFavoritarCor(favoritado: Boolean)
        fun showToast(message: Int)
    }
}