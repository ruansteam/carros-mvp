package br.com.mvp.carros.carrosmvp.features.login.service.http

import br.com.mvp.carros.carrosmvp.features.login.domain.vo.UsuarioVO
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Ruan Correa on 04/01/18.
 */
interface LoginService {
    @GET("users")
    fun doPostLogin(@Query("username") user: String): Observable<List<UsuarioVO>>
}