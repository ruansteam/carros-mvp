package br.com.mvp.carros.carrosmvp.features.carros.listar.presenter

import android.os.Bundle
import android.widget.ImageView
import br.com.mvp.carros.carrosmvp.common.constants.BundleConstants
import br.com.mvp.carros.carrosmvp.common.constants.WebServicesConstants
import br.com.mvp.carros.carrosmvp.common.util.createRetrofitService
import br.com.mvp.carros.carrosmvp.features.carros.listar.adapter.TipoCarro
import br.com.mvp.carros.carrosmvp.features.carros.listar.domain.vo.CarroVO
import br.com.mvp.carros.carrosmvp.features.carros.listar.service.http.CarrosService
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlin.properties.Delegates

/**
 * Created by Ruan Correa on 05/01/18.
 */
class CarrosPresenter(private val viewCallback: CarrosPresenter.ViewCallback) {

    private lateinit var tipo: TipoCarro
    private var observerCars: Observable<List<CarroVO>>? = null

    private val service: CarrosService by lazy {
        createRetrofitService(WebServicesConstants.URL_CARROS).create(CarrosService::class.java)
    }
    private var listCars: MutableList<CarroVO> by Delegates.observable(mutableListOf()) { prop, old, new ->
        updateAdapter(new, { carro, imageView -> viewCallback.showDetalhesCarroActivity(carro, imageView)})
    }

    fun onViewCreated(view: android.view.View?, savedInstanceState: Bundle?, arguments: Bundle?) {
        this.viewCallback.setupViews()

        tipo = if (arguments != null) {
            arguments.getSerializable(BundleConstants.TIPO) as TipoCarro
        } else {
            TipoCarro.CLASSICOS
        }

        createObserverIfNecessary()

        subscribeObserver()
    }

    fun swipeRefreshListener() {
        subscribeObserver()
    }

    private fun createObserverIfNecessary() {
        if (observerCars == null) {
            observerCars = service.getCarro(tipo.name)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
        }
    }

    private fun updateAdapter(list: MutableList<CarroVO>, function: (CarroVO, ImageView) -> Unit) {
        viewCallback.updateAdapter(list, function)
    }

    private fun subscribeObserver() {
        observerCars?.subscribeBy(onNext = { list ->
            listCars = list.toMutableList()
            viewCallback.closeSwipe()
        }, onError = {
            viewCallback.closeSwipe()
        })
    }

    interface ViewCallback {
        fun setupViews()
        fun updateAdapter(list: MutableList<CarroVO>, function: (CarroVO, ImageView) -> Unit)
        fun closeSwipe()
        fun showDetalhesCarroActivity(carro: CarroVO, imageView: ImageView)
    }

}