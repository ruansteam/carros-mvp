package br.com.mvp.carros.carrosmvp.features.login.domain.vo

/**
 * Created by Ruan Correa on 04/01/18.
 */
data class UsuarioVO(val id: Long,
                     var username: String,
                     var name: String,
                     var email: String,
                     var phone: String? = null,
                     var website: String? = null)