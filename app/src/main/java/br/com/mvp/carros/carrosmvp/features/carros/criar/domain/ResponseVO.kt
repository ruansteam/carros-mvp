package br.com.mvp.carros.carrosmvp.features.carros.criar.domain

/**
 * Created by macpr on 09/01/18.
 */
data class ResponseVO (val id:Long,val status:String,val msg:String,val url:String) {
    fun isOk() = "OK".equals(status, ignoreCase = true)
}