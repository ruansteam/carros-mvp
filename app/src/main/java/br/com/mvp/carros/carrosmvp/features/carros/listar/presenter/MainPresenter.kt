package br.com.mvp.carros.carrosmvp.features.carros.listar.presenter

import android.os.Bundle

/**
 * Created by Ruan Correa on 05/01/18.
 */
class MainPresenter(private val viewCallback: MainPresenter.ViewCallback) {

    fun onCreate(savedInstanceState: Bundle?) {
        viewCallback.setupMenuButton()
        viewCallback.setupTabBar()
    }

    fun onClickFab() {
        viewCallback.showCriarCarro()
    }

    interface ViewCallback {
        fun setupMenuButton()
        fun setupTabBar()
        fun showCriarCarro()
    }
}