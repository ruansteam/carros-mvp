package br.com.mvp.carros.carrosmvp.features.carros.criar.presenter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.content.FileProvider
import android.support.v7.app.AppCompatActivity
import android.util.Log
import br.com.livroandroid.camera.ImageUtils
import br.com.mvp.carros.carrosmvp.R
import br.com.mvp.carros.carrosmvp.application.CarrosApplication
import br.com.mvp.carros.carrosmvp.common.constants.BundleConstants
import br.com.mvp.carros.carrosmvp.features.carros.criar.services.http.CriarCarroService
import br.com.mvp.carros.carrosmvp.features.carros.listar.adapter.TipoCarro
import br.com.mvp.carros.carrosmvp.features.carros.listar.domain.vo.CarroVO
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.toast
import java.io.File
import java.io.FileOutputStream
import java.nio.file.Files.exists

/**
 * Created by macpr on 09/01/18.
 */
class CriarCarroPresenter(val viewCallback: CriarCarroPresenter.ViewCallback) {

    var carro: CarroVO? = null
    var file: File? = null

    fun onCreate(savedInstanceState: Bundle?, extras: Bundle? = null) {
        carro =  extras?.getParcelable(BundleConstants.BUNDLE_CARRO)
        viewCallback.setupToolbar()
    }

    fun onClickBarImg() {
        val ms = System.currentTimeMillis()
        // Nome do arquivo da foto
        val fileName = if (carro != null) "foto_carro_${carro?.id}.jpg" else "foto_carro_${ms}.jpg"
        // Abre a câmera
        file = getSdCardFile(CarrosApplication.instance.applicationContext, fileName)
        Log.d("camera", file.toString())
        viewCallback.showCamera(fileName, file!!)
    }

    fun getSdCardFile(context: Context, fileName: String): File {
        val dir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        if (!dir.exists()) {
            dir.mkdir()
        }
        return File(dir, fileName)
    }

    fun save(bitmap: Bitmap) {
        file?.apply {
            val out = FileOutputStream(this)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out)
            out.close()
        }
    }

    fun getBitmap(w: Int, h: Int): Bitmap? {
        file?.apply {
            if (exists()) {
                val bitmap = ImageUtils.resize(this, w, h)
                return bitmap
            }
        }
        return null
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            val bitmap = getBitmap(600, 600)
            if (bitmap != null) {
                save(bitmap)
                viewCallback.setImageBitmap(bitmap)
            }
        }
    }

    fun salvarCarro(nome: String, desc: String, radioId: Int) {
        if (nome.isEmpty() || desc.isEmpty()) {
            viewCallback.showErrorInParameters()
            return
        }
        val tipo = when (radioId) {
            R.id.tipoClassico -> TipoCarro.CLASSICOS.name
            R.id.tipoEsportivo -> TipoCarro.ESPORTIVOS.name
            else -> TipoCarro.LUXO.name
        }

        viewCallback.showProgress()
        file?.let { file ->
            Observable.fromCallable { CriarCarroService.criarCarro(nome = nome, desc = desc, tipo = tipo, file = file) }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeBy(onNext = {
                        viewCallback.carroCriado(it.msg)
                    }, onError = {
                        viewCallback.showError(it.message)
                    })
        }
    }

    interface ViewCallback {
        fun setupToolbar()
        fun setImageBitmap(bitmap: Bitmap)
        fun showErrorInParameters()
        fun showCamera(fileName: String, file: File)
        fun carroCriado(msg: String)
        fun showError(message: String?)
        fun showProgress()
    }
}
