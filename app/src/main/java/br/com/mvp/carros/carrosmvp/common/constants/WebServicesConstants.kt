package br.com.mvp.carros.carrosmvp.common.constants

/**
 * Created by Joao on 17/01/2018.
 */
class WebServicesConstants {
    companion object {
        const val URL_LOGIN = "https://jsonplaceholder.typicode.com/"
        const val URL_CARROS = "http://livrowebservices.com.br/rest/carros/"
    }
}