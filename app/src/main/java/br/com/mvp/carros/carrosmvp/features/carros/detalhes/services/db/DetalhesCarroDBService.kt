package br.com.mvp.carros.carrosmvp.features.carros.detalhes.services.db

import br.com.mvp.carros.carrosmvp.application.CarrosApplication
import br.com.mvp.carros.carrosmvp.database.CarrosDatabase
import br.com.mvp.carros.carrosmvp.database.entitiy.Carro
import br.com.mvp.carros.carrosmvp.features.carros.listar.domain.vo.CarroVO

/**
 * Created by Ruan Correa on 08/01/18.
 */
class DetalhesCarroDBService {
    companion object {
        private fun getDb(): CarrosDatabase {
            return CarrosApplication.instance.db ?: throw Exception("Banco de dados com erro")

        }

        fun favoritarOuDesfavoritar(carroVO: CarroVO): Boolean {
            val carro = getDb().carroDAO().getById(carroVO.id)
            return if (carro == null) {
                val new = Carro.from(carroVO)
                getDb().carroDAO().insert(new)
                true
            } else {
                getDb().carroDAO().delete(carro)
                false
            }
        }

        fun isFavorito(carroVO: CarroVO): Boolean {
            return getDb().carroDAO().isFavorito(carroVO.id)
//            var carro = getDb().carroDAO().getById(carroVO.id)
//            return carro == null
        }
    }
}