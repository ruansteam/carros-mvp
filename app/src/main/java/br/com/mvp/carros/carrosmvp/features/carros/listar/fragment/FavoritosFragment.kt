package br.com.mvp.carros.carrosmvp.features.carros.listar.fragment

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import br.com.mvp.carros.carrosmvp.R
import br.com.mvp.carros.carrosmvp.common.constants.BundleConstants
import br.com.mvp.carros.carrosmvp.features.carros.detalhes.activity.DetalhesCarroActivity
import br.com.mvp.carros.carrosmvp.features.carros.listar.adapter.CarroAdapter
import br.com.mvp.carros.carrosmvp.features.carros.listar.domain.vo.CarroVO
import br.com.mvp.carros.carrosmvp.features.carros.listar.presenter.FavoritosPresenter
import kotlinx.android.synthetic.main.fragment_favoritos.*
import org.jetbrains.anko.support.v4.toast

class FavoritosFragment : Fragment(), FavoritosPresenter.ViewCallback {

    private var adapter: CarroAdapter? = null
    private val presenter: FavoritosPresenter by lazy { FavoritosPresenter(this) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_favoritos, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onViewCreated(view, savedInstanceState)
    }

    override fun setupViews() {
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.setHasFixedSize(true)

        swipeToRefresh.setOnRefreshListener {
            presenter.swipeToRefreshListener()
        }
    }

    override fun updateAdapter(list: MutableList<CarroVO>, action: (CarroVO, ImageView) -> Unit) {
        if (adapter == null) {
            adapter = CarroAdapter(context, list, action)
            recyclerView.adapter = adapter
        } else {
            adapter?.carros = list
        }
        adapter?.notifyDataSetChanged()
    }

    override fun closeSwipeRefresh() {
        swipeToRefresh.isRefreshing = false
    }

    override fun showOrHideSemRegistros(visible: Int) {
        tvSemRegistro.visibility = visible
    }

    override fun showToastError(erro: Int) {
        toast(R.string.erro)
    }

    override fun showActivityDetalhesCarro(carro: CarroVO, imageView: ImageView) {
        var intent = Intent(activity, DetalhesCarroActivity::class.java)
        intent.putExtra(BundleConstants.BUNDLE_CARRO, carro)
        startActivity(intent)
    }
}
