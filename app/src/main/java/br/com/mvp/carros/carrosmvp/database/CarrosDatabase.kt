package br.com.mvp.carros.carrosmvp.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import br.com.mvp.carros.carrosmvp.database.dao.CarroDAO
import br.com.mvp.carros.carrosmvp.database.entitiy.Carro

/**
 * Created by Ruan Correa on 05/01/18.
 */
@Database(entities = arrayOf(Carro::class), version = 2)
abstract class CarrosDatabase: RoomDatabase() {
    abstract fun carroDAO(): CarroDAO
}