package br.com.mvp.carros.carrosmvp.features.login.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import br.com.mvp.carros.carrosmvp.R
import br.com.mvp.carros.carrosmvp.features.carros.listar.activity.MainActivity
import br.com.mvp.carros.carrosmvp.features.login.presenter.LoginPresenter
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.startActivity

class LoginActivity : AppCompatActivity(), LoginPresenter.ViewCallback {

    private val presenter: LoginPresenter by lazy { LoginPresenter(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        presenter.onCreate(savedInstanceState)

        btnLogin.setOnClickListener { presenter.onClickLogin(etEmail.text.toString(), etSenha.text.toString()) }
    }

    override fun showProgress() {
        progressBar.visibility = View.VISIBLE
        progressBar.isIndeterminate = true
    }

    override fun hideProgress() {
        progressBar.visibility = View.GONE
    }

    override fun disableFields() {
        etEmail.isEnabled = false
        etSenha.isEnabled = false
        btnLogin.isEnabled = false
    }

    override fun enableFields() {
        etEmail.isEnabled = true
        etSenha.isEnabled = true
        btnLogin.isEnabled = true
    }

    override fun setTitle(titleResId: Int) {
        this.title = getString(titleResId)
    }

    override fun showError(messageResId: Int) {
        alert(messageResource = messageResId, titleResource = R.string.error).show()
    }

    override fun showMain() {
        startActivity<MainActivity>()
    }

}