package br.com.mvp.carros.carrosmvp.features.carros.criar.activity

import android.content.Intent
import android.graphics.Bitmap
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.content.FileProvider
import android.view.Menu
import android.view.MenuItem
import br.com.mvp.carros.carrosmvp.R
import br.com.mvp.carros.carrosmvp.features.carros.criar.presenter.CriarCarroPresenter
import kotlinx.android.synthetic.main.activity_criar_carro.*
import org.jetbrains.anko.toast
import java.io.File

class CriarCarroActivity : AppCompatActivity(), CriarCarroPresenter.ViewCallback {


    val presenter: CriarCarroPresenter by lazy { CriarCarroPresenter(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_criar_carro)
        presenter.onCreate(savedInstanceState = savedInstanceState)

        img.setOnClickListener { presenter.onClickBarImg() }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        presenter.onActivityResult(requestCode = requestCode, resultCode = resultCode, data = data)
    }

    // Adiciona as opções Salvar e Deletar no menu
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_form_carro, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_salvar -> presenter.salvarCarro(nome = tNome.text.toString(), desc = tDesc.text.toString(), radioId = radioTipo.checkedRadioButtonId)
        }
        return true
    }

    override fun setupToolbar() {
        setSupportActionBar(toolbar)
        title = getString(R.string.novo_carro)
        supportActionBar?.title = getString(R.string.novo_carro)
    }

    override fun setImageBitmap(bitmap: Bitmap) {
        img.setImageBitmap(bitmap)
    }

    override fun showErrorInParameters() {
        if (tNome.text.isEmpty()) {
            // Valida se o campo nome foi preenchido
            tNome.error = getString(R.string.msg_error_form_nome)
        }
        if (tDesc.text.isEmpty()) {
            // Valida se o campo descrição foi preenchido
            tDesc.error = getString(R.string.msg_error_form_desc)
        }
    }

    override fun carroCriado(msg: String) {
        toast(msg)
        finish()
    }

    override fun showError(message: String?) {
        toast(message ?: getString(R.string.erro))
        finish()
    }

    override fun showCamera(fileName: String, file: File) {
        val intent = open(fileName, file)
        startActivityForResult(intent, 0)
    }

    override fun showProgress() {

    }

    fun open(fileName: String, file: File): Intent {
        val i = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val uri = FileProvider.getUriForFile(this, applicationContext.packageName + ".provider", file)
        i.putExtra(MediaStore.EXTRA_OUTPUT, uri)
        return i

    }


}
