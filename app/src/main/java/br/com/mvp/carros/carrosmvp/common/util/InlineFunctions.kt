package br.com.mvp.carros.carrosmvp.common.util

import android.content.Context
import br.com.mvp.carros.carrosmvp.application.CarrosApplication
import br.com.mvp.carros.carrosmvp.database.CarrosDatabase
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by macpr on 09/01/18.
 */
inline fun createRetrofitService(url: String): Retrofit =
        Retrofit
                .Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(url)
                .build()

inline fun getDB(): CarrosDatabase? = CarrosApplication.instance.db