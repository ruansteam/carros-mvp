package br.com.mvp.carros.carrosmvp.features.carros.listar.service.http

import br.com.mvp.carros.carrosmvp.features.carros.listar.domain.vo.CarroVO
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by Ruan Correa on 05/01/18.
 */
interface CarrosService {
    @GET("tipo/{tipo}")
    fun getCarro(@Path("tipo") tipo: String): Observable<List<CarroVO>>
}