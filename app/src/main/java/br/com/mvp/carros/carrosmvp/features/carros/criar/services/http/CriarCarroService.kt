package br.com.mvp.carros.carrosmvp.features.carros.criar.services.http

import android.util.Base64
import br.com.mvp.carros.carrosmvp.common.constants.WebServicesConstants
import br.com.mvp.carros.carrosmvp.common.util.createRetrofitService
import br.com.mvp.carros.carrosmvp.database.entitiy.Carro
import br.com.mvp.carros.carrosmvp.features.carros.criar.domain.ResponseVO
import br.com.mvp.carros.carrosmvp.features.carros.listar.domain.vo.CarroVO
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import java.io.File

/**
 * Created by macpr on 09/01/18.
 */
class CriarCarroService {
    interface Retrofit {
        @FormUrlEncoded
        @POST("postFotoBase64")
        fun postFoto(@Field("fileName") fileName:String, @Field("base64") base64:String): Call<ResponseVO>

        @POST()
        fun postCarro(@Body carro: CarroVO): ResponseVO
    }
    companion object {
        fun criarCarro(nome: String, desc: String, tipo: String, file: File): ResponseVO {
            val service = createRetrofitService(WebServicesConstants.URL_CARROS).create(CriarCarroService.Retrofit::class.java)
            // Converte para Base64
            val bytes = file.readBytes()
            val base64 = Base64.encodeToString(bytes, Base64.NO_WRAP)

            val call = service.postFoto(file.name,base64)
            val responseFoto = call.execute().body()

            if (responseFoto?.isOk() == true) {
                var carro = CarroVO()
                carro.nome = nome
                carro.desc = desc
                carro.tipo = tipo
                carro.urlFoto = responseFoto.url

                var response = service.postCarro(carro)
                return response
            }

            throw Exception("Erro ao criar carro")
        }
    }
}