package br.com.mvp.carros.carrosmvp.features.carros.listar.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import br.com.mvp.carros.carrosmvp.R
import br.com.mvp.carros.carrosmvp.features.carros.listar.domain.vo.CarroVO
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.adapter_carro.view.*


class CarroAdapter(val context: Context? = null,
                   var carros: List<CarroVO>,
                   val onClick: (CarroVO, ImageView) -> Unit) : RecyclerView.Adapter<CarroAdapter.CarrosViewHolder>() {

    init {
        setHasStableIds(true)
    }

    override fun getItemCount(): Int = this.carros.size

    override fun getItemId(position: Int): Long = this.carros[position].id

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarrosViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.adapter_carro, parent, false)
        return CarrosViewHolder(view)
    }

    // Faz o bind para atualizar o valor das views com os dados do Carro
    override fun onBindViewHolder(holder: CarrosViewHolder, position: Int) {
        // Recupera o objeto carro
        val carro = carros[position]
        val view = holder.itemView
        with(view) {
            // Atualiza os dados do carro
            tNome.text = carro.nome
            // Faz o download da foto e mostra o ProgressBar
//            img.loadUrl(carro.urlFoto, progress)
            Glide.with(context).load(carro.urlFoto).into(imgVideo)
            // Adiciona o evento de clique na linha
            setOnClickListener { onClick(carro, imgVideo) }
        }
    }

    // ViewHolder fica vazio pois usamos o import do Android Kotlin Extensions
    class CarrosViewHolder(view: View) : RecyclerView.ViewHolder(view)
}
