package br.com.mvp.carros.carrosmvp.features.carros.listar.fragment


import android.content.Intent
import android.os.Bundle


import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import br.com.mvp.carros.carrosmvp.R
import br.com.mvp.carros.carrosmvp.R.id.recyclerView
import br.com.mvp.carros.carrosmvp.R.id.swipeToRefresh
import br.com.mvp.carros.carrosmvp.common.constants.BundleConstants
import br.com.mvp.carros.carrosmvp.features.carros.detalhes.activity.DetalhesCarroActivity

import br.com.mvp.carros.carrosmvp.features.carros.listar.adapter.CarroAdapter
import br.com.mvp.carros.carrosmvp.features.carros.listar.domain.vo.CarroVO
import br.com.mvp.carros.carrosmvp.features.carros.listar.presenter.CarrosPresenter
import kotlinx.android.synthetic.main.fragment_favoritos.*
/**
 * A simple [Fragment] subclass.
 */
class CarrosFragment : Fragment(), CarrosPresenter.ViewCallback {

    private val presenter: CarrosPresenter by lazy { CarrosPresenter(this) }
    private var adapter: CarroAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
            = inflater.inflate(R.layout.fragment_carros, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onViewCreated(view = view, savedInstanceState = savedInstanceState, arguments = arguments)
    }

    override fun updateAdapter(list: MutableList<CarroVO>, function: (CarroVO, ImageView) -> Unit) {
        if(adapter == null) {
            adapter = CarroAdapter(context, list, function)
            recyclerView.adapter = adapter
        } else {
            adapter?.carros = list
        }
        adapter?.notifyDataSetChanged()
    }

    override fun setupViews() {
        // Views
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.setHasFixedSize(true)

        // Swipe to Refresh
        swipeToRefresh.setOnRefreshListener {
            presenter.swipeRefreshListener()
        }
    }

    override fun closeSwipe() {
        swipeToRefresh.isRefreshing = false
    }

    override fun showDetalhesCarroActivity(carro: CarroVO, imageView: ImageView) {
        var intent = Intent(activity, DetalhesCarroActivity::class.java)
        intent.putExtra(BundleConstants.BUNDLE_CARRO, carro)
        startActivity(intent)
    }

}
